module.exports = {
  apps: [{
    name: `webarchivewebserver-${process.env.NODE_ENV}`,
    script: 'npm start',
    autorestart: true,
  }],
};