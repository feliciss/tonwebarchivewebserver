'use strict'

const fp = require('fastify-plugin')
const TonstorageCLI = require('tonstorage-cli');

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async function (fastify, opts) {
  fastify.decorate('initStorageCli', function () {
    return new TonstorageCLI({
        bin: process.env.TONSTORAGE_CLI_BIN,
        host: process.env.TONSTORAGE_CLI_HOST,
        database: process.env.TONSTORAGE_CLI_DATABASE,
        timeout: Number(process.env.TONSTORAGE_CLI_TIMEOUT),
      });
  })
})
