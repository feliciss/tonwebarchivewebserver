'use strict'

const { test } = require('tap')
const Fastify = require('fastify')
const Storagecli = require('../../plugins/storagecli')

test('support works standalone', (t) => {
  t.plan(2)
  const fastify = Fastify()
  fastify.register(Storagecli)

  fastify.ready((err) => {
    t.error(err)
    t.ok(fastify.initStorageCli())
  })
})