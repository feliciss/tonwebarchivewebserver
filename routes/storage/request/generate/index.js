'use strict'

module.exports = async function (fastify, opts) {
  fastify.post('/', async function (request, reply) {
    const CLI = fastify.initStorageCli();
    const newContractMessage = await CLI.newContractMessage(
        request.body.bagId,
        0,
        request.body.storageProviderAddress,
    );
    return newContractMessage
  })
}
